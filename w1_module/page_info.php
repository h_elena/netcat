<?php
/**
 * The output page template information in the admin.
 */
new w1_admin();
?>
<div id="tab_info">
  <div class="media">
      <div class="media-left">
          <a href="#">
              <img class="media-object" src="<?php echo $this->MODULE_PATH?>/walletone/img/w1_logo.png" alt="Wallet One Единая касса">
          </a>
          <p>Универсальный платежный агрегатор, предлагающий более 100 способов приема платежей.</p>
          <ul class="list-characteristic">
              <li><i>&#10004;</i>Быстрое подключение</li>
              <li><i>&#10004;</i>Простая интеграция</li>
              <li><i>&#10004;</i>Наглядная аналитика</li>
              <li><i>&#10004;</i>Круглосуточная служба поддержки</li>
          </ul>
      </div>
      <div class="media-body">
          <blockquote>
              <p><span>Быстрая настройка</span></p>
              <ul class="list-settings-ul">
                  <li><div class="circle-fgnghn">1</div><span>Зарегистрироваться в кассе на <a target="_blank" href="https://www.walletone.com/ru/merchant/">сайте</a></span></li>
                  <li><div class="circle-fgnghn">2</div><span>Заполнить все обязательные поля из раздела "Настройки".</span></li>
                  <li><div class="circle-fgnghn">3</div><span>Для вывода определенных способов оплаты, заполните поля в разделе "Доп. настройки".</span></li>
              </ul>
              <ul class="app_links">
                  <li><a href="https://itunes.apple.com/ru/app/merchant-w1/id925378607?mt=8" class="app_link"></a></li>
                  <li><a href="https://play.google.com/store/apps/details?id=com.w1.merchant.android" class="app_link __android"></a></li>
              </ul>
              <div class="clearfix"></div>
          </blockquote>
      </div>
  </div>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->MODULE_PATH?>walletone/css/netcat.css?v=3">
</div>