<?php
/**
 * Class output menu in the admin
 */
class ui_config_module_w1 extends ui_config_module {
  public $headerText = W1;
  /**
   * The output settings in the admin.
   * @param type $view
   * @param type $params
   */
	function ui_config_module_w1 ($view, $params) {
    include_once __DIR__.'/walletone/Classes/W1Client.php';
    $client = \WalletOne\Classes\W1Client::init()->run(MAIN_LANG, 'netcat');
    
		$this->tabs[] = array(
			'id' => "info",
			'caption' => w1SettingsInfoTab,
			'location' => "module.w1.info",
			'group' => "admin"
		);
		$this->tabs[] = array(
			'id' => "settings",
			'caption' => w1Settings,
			'location' => "module.w1.settings",
			'group' => "admin"
		);

		$this->activeTab = $view;
		$this->locationHash = "module.w1.".$view . ($params ? "(".$params.")" : "");
		$this->treeMode = "modules";

		$module_settings = nc_Core::get_object()->modules->get_by_keyword('w1');
		$this->treeSelectedNode = "module-".$module_settings['Module_ID'];
	}
  
  /**
   * 
   */
	public function add_settings_toolbar() {
		$this->activeTab = 'settings';
  }
}


