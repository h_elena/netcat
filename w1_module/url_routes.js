urlDispatcher.addRoutes({
    'module.w1': NETCAT_PATH + 'modules/w1/admin.php?view=info'
})
.addPrefixRouter('module.w1.', function (path, params) {
    var view = path.split('.');
    var url = NETCAT_PATH + "modules/w1/admin.php?view=" + view[view.length-1];
    if (params) {
        url += "&id=" + params;
    }
    mainView.loadIframe(url);
});