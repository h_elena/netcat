<?php
/**
 * The output page template settings in the admin.
 */

new w1_admin();
?>
<form method="post" action="admin.php" id="MainSettigsForm" style="padding:0; margin:0;">
	<input type="hidden" name="view" value="settings" />
	<input type="hidden" name="act" value="save" />
  <div> 
    <?php 
    foreach ($this->form as $key => $field) {
      echo '<div style="margin:10px 0;padding:0;">
                  <label style="width: 400px;display: block;float: left;margin-right: 10px;" title="'.$field['placeholder'].'">'.$field['label'].'</label>';
      switch ($field['type']) {
        case 'input':
          echo '<input type="text" value="'.$field['value'].'" name="'.$key.'" size="50" maxlength="250">';
          break;
        case 'select':
          echo '<select name="'.$key.'">';
          foreach ($field['options'] as $key => $val) {
            echo '<option value="' . $key . '"';
            if ($field['value'] == $key) {
              echo ' selected ';
            }
            echo '>' . $val . '</option>';
          }
          echo '</select>';
          break;
        case 'groupCheckboxes':
          echo $field['html'];
          break;
        case 'checkbox':
          echo '<input type="checkbox" value="1" name="'.$key.'"'.($field['value'] == 1 ? ' checked' : '').'>';
          break;
      }
      echo '<div style="clear: both"></div>'
      . '</div>';
    }?>
  </div>
</form>
<link rel="stylesheet" type="text/css" href="<?php echo $this->MODULE_PATH?>walletone/css/netcat.css?v=5">
<?php $UI_CONFIG->actionButtons[] = array(
	"id" => "submit",
	"caption" => w1SettingsSubmit,
	"action" => "mainView.submitIframeForm('MainSettingsForm')"
);
?>